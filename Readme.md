# Sources



## NISEAR 특수교육원 모바일앱

https://drive.google.com/file/d/1QnHUj5uZeYNnffZqbEk7gWgYKeeex6kC/view?usp=drive_web

*개발소스 스펙입니다.
front-end : sencha-touch 2.4.2
package-tool : cordova
네이티브 주요 적용 기술(java)
- nodejs서버, zip압축해제, sqlite디바이스저장소, 파일다운로더, 인앱브라우져
- 

## 홀로매직 와이파이앱 소스

https://drive.google.com/file/d/1Xuvmi-Z53ftAa46L_SNWlm-184JreJGg/view?usp=drive_web
package-tool : cordova

- 키스토어파일 및 정보는 안드로이드소스폴더내에 keystore폴더내에 있음



## 3DBANK 모바일앱 소스

https://drive.google.com/file/d/1ZX0l2rz7d56v9byb6a4GEvBLSE8J5H7e/view?usp=drive_web

- 스토어에 배포하지 않은 안드로이드앱소스임.
